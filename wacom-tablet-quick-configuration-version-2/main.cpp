#include "mainwindow.h"
#include <iostream>
#include <unistd.h>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    if(argc > 1){
        switch(getopt(argc, argv, "av1")){
        case 'a':
            printf("./Ashers Wacom Tablet Quick Configuration Version 2 \n\r Created by AshersPrograms.com 2023 \n\r");
            exit(0);
        break;
        case 'v':
            printf("2.01\n\r");
            exit(0);
        break;
        }
    }

    w.show();
    return a.exec();
}
