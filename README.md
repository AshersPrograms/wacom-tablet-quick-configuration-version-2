# Wacom Tablet Quick Configuration Version 2

<img src="wacom-tablet-quick-configuration-version-2/images/ashersprograms.png" alt="Company Logo" width="200"/>

![Program Logo](wacom-tablet-quick-configuration-version-2/images/wacomquickconfiguration_v2.png)

## Overview

The **Wacom Tablet Quick Configuration Version 2** is a Linux desktop front-end application designed specifically for Wacom Drawing tablets. This application allows users to easily configure their Wacom Drawing tablet settings, offering a more efficient and personalized drawing experience. The user-friendly interface makes it simple for users of all skill levels, from beginners to experienced artists, to utilize the application and enhance their digital drawing workflow.

## Features

- **Easy Configuration**: Simplifies the setup and configuration of Wacom Drawing tablets.
- **User-Friendly Interface**: Intuitive design for ease of use.
- **Enhanced Drawing Experience**: Streamlines workflow for both beginners and experienced artists.

## Updates for Version 2

- **Smaller Footprint: The application is optimized to use minimal system resources, ensuring it runs smoothly on your device without impacting performance.
- **4 Presets for Your Favorite Settings: Quickly switch between your favorite configurations with four customizable presets.
- **Works with Your Default Browser: Unlike other tools that only support Firefox, this application works seamlessly with your default web browser.
- **Saves Exact Position: The application remembers the exact position of your windows, ensuring a consistent user experience every time you open it.
- **Updated Explanations: We've improved the documentation to help users better understand the functions and features of the application.


## Getting Started

For more information, visit the [program page](https://ashersprograms.com/?program=wacom_v2), which includes:

- [Getting Started Guide](https://ashersprograms.com/index.php?program=wacom_v2#GettingStarted)
- [Keyboard Shortcuts](https://ashersprograms.com/?program=wacom_v2)
- [Credits](https://ashersprograms.com/index.php?program=wacom_v2#Credits)

Watch the [accompanying video](https://youtu.be/BTGjppn4cuQ) to learn about the program's options and capabilities with the Wacom Drawing Tablet.

## Dependencies

- **libqt5widgets5**: This is a QT5 configuration file.
- **xsetwacom**: This program serves as a front end for the configuration tool.

## Social Media

Connect with us on [Facebook](https://www.facebook.com/groups/648648643592658).

## Support

If you enjoy using the program, consider supporting the developer on [Patreon](https://www.patreon.com/user?u=74944932).

## License

This project is licensed under the [MIT License](LICENSE).

